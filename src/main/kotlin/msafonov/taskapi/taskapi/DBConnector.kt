package msafonov.taskapi.taskapi

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import java.sql.*

class DBConnector (private val dbName: String){
    private var conn: Connection? = null
    private var statmt: Statement? = null
    private var resSet: ResultSet? = null
    private val logger: Logger = LoggerFactory.getLogger(DBConnector::class.java)

    // Подключение/создание БД и таблицы
    init {
        conn = null
        Class.forName("org.sqlite.JDBC")
        conn = DriverManager.getConnection("jdbc:sqlite:$dbName.s3db")
        logger.info("Сonnection established with $dbName")
        statmt = conn?.createStatement()
        statmt?.execute("CREATE TABLE if not exists 'USER_TASKS' ('GUID' VARCHAR(32) UNIQUE NOT NULL, 'DATE' DATE NOT NULL, 'STATUS' VARCHAR(16) NOT NULL); ")
    }

    // Добавление записи задания
    fun addTask(uuid: String, date: String) {
        statmt?.execute("INSERT INTO USER_TASKS VALUES ('$uuid', '$date', 'created'); ")
        logger.debug("New task with UUID $uuid added")
    }

    // Обновление записи задания
    fun updateTask(uuid: String, date: String, status: String){
        statmt?.execute("UPDATE USER_TASKS SET DATE = '$date', STATUS = '$status' WHERE GUID = '$uuid'; ")
        logger.debug("Task with UUID $uuid is now $status")
    }

    fun getTaskObject(uuid: String): TaskObject?{
        val resultSet = statmt?.executeQuery("SELECT * FROM USER_TASKS WHERE GUID='$uuid'")
        var taskObject:TaskObject? = null
        try {
            taskObject = TaskObject(resultSet!!.getString("STATUS"),resultSet.getString("DATE"))
        } catch (e: SQLException){
            logger.debug("Tried to acces non-existent UUID")
        }
        return taskObject
    }

    // Закрытие соединения
    fun CloseDB() {
        conn!!.close()
        statmt?.close()
        resSet?.close()
        logger.info("Connection closed with $dbName")
    }
}