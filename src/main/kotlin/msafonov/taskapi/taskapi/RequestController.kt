package msafonov.taskapi.taskapi

import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletRequest

@RestController
class RequestController {
    val dbConnector = DBConnector("task_db")
    private val logger: Logger = LoggerFactory.getLogger(RequestController::class.java)

    @RequestMapping(value = ["/task"], method = [RequestMethod.POST])
    fun newTask(request: HttpServletRequest): ResponseEntity<String> {
        val task = Task(dbConnector)
        val thread = Thread(task)
        logger.info("Got POST request from ${request.remoteAddr} ; created task with UUID ${task.uuid}")
        thread.start()
        return ResponseEntity.status(202).body(task.uuid!!)
    }

    @RequestMapping(value = ["/task/{id}"], method = [RequestMethod.GET])
    fun taskStatus(@PathVariable("id") uuid: String, request: HttpServletRequest): ResponseEntity<TaskObject> {
        if (!UUIDVerifier.verify(uuid)){
            logger.info("Got GET request from ${request.remoteAddr} with non-UUID parameter ($uuid)")
            return ResponseEntity.status(400).body(null)
        }
        val taskObject = dbConnector.getTaskObject(uuid)

        if (taskObject == null) {
            logger.info("Got GET request from ${request.remoteAddr} with nonexistent UUID ($uuid)")
            return ResponseEntity.status(404).body(null)
        } else
            logger.info("Got GET request from ${request.remoteAddr} for UUID $uuid")
            return ResponseEntity.status(200).body(taskObject)
    }


}