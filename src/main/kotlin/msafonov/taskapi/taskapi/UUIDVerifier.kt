package msafonov.taskapi.taskapi

import java.util.regex.Pattern

object UUIDVerifier {
    val pattern = Pattern.compile("^[0-9a-fA-F]{8}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{4}-[0-9a-fA-F]{12}\$")

    // Возвращает true, если строка является UUID
    fun verify(id: String): Boolean{
        val matcher = pattern.matcher(id)
        return matcher.matches()
    }
}