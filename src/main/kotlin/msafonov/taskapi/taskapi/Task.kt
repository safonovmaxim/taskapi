package msafonov.taskapi.taskapi

import java.nio.ByteBuffer
import java.text.SimpleDateFormat
import java.util.*

class Task(private val dbConnector: DBConnector, private val duration: Long = 120000): Runnable {
    var uuid:String? = null
    private val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")

    init {
        val buffer = ByteBuffer.allocate(java.lang.Long.BYTES)
        buffer.putLong(System.currentTimeMillis()+System.nanoTime())
        uuid = UUID.randomUUID().toString()
        val date = Date()
        dbConnector.addTask(uuid!!,dateFormat.format(date))
    }

    override fun run() {
        var date = dateFormat.format(Date())
        dbConnector.updateTask(uuid!!, date,"running")
        Thread.sleep(duration)
        date = dateFormat.format(Date())
        dbConnector.updateTask(uuid!!, date,"finished")
    }
}