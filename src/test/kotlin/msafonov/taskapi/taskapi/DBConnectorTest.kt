package msafonov.taskapi.taskapi

import org.junit.Before
import org.junit.Test
import java.util.*

class DBConnectorTest {
    var dbConnector: DBConnector? = null
    val uuid = UUID.randomUUID().toString()

    @Before
    fun setup(){
        dbConnector = DBConnector("task_db_test")
        dbConnector?.addTask(uuid,"2018-08-14 22:11:34.124")
    }

    @Test
    fun testConnector(){
        dbConnector?.updateTask(uuid,"2018-08-14 23:11:34.124", "finished")
        dbConnector?.getTaskObject(uuid)
    }
}