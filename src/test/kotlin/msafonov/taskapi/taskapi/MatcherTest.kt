package msafonov.taskapi.taskapi

import junit.framework.Assert.assertFalse
import junit.framework.Assert.assertTrue
import org.junit.Test
import java.util.*

class UUIDVerifierTest {
    @Test
    fun testVerify(){
        assertFalse(UUIDVerifier.verify("testtestnotUUID"))
        assertTrue(UUIDVerifier.verify("01234567-9ABC-DEF0-1234-56789ABCDEF0"))
        assertTrue(UUIDVerifier.verify(UUID.randomUUID().toString()))
    }
}