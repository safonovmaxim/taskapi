package msafonov.taskapi.taskapi

import org.junit.Test

class TaskTest {

    @Test
    fun testRun(){
        val dbConnector = DBConnector("task_db_test")
        val taskDuration = 1L
        val task = Task(dbConnector,taskDuration)
        task.run()
    }
}